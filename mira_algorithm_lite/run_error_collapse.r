library(sequenceCollapser)
library(dplyr)
library(purrr)
library(data.table)

## Run error collapsing on immunoSEQ MIRA data. 
# Steps:
# 1. Run error collapse using read counts.
# 2. Make error collapsed sequence info files reads
# 3. Make error collapsed sequence info files for templates
# Output:
# sequence info files for reads and templates
# error map
# well name map, which contains mappings from well number to well name (3 -> antigen_pool_G)
# unique sequence num pre and post error collapse

## Input args
tsv_paths_str <- ""             # [REQUIRED] comma-delimited string of tsv paths
output_dir <- ""                # [REQUIRED] directory for output files 
cdr3_distance_fraction <- .035  # <optional> fraction of CDR3 bases that may differ between collapsed sequences
min_reads <- 1                  # <optional> require at least this many reads per seq per well

## -------------------------------------
## ---> INTERNAL UTILITY FUNCTIONS <----
## -------------------------------------

## function for parsing command line arguments: first two inputs are character
## vectors embedded in lists; third input is environment of the calling context
ProcessCommandArgs <- function(command.args, variables, parent.env) {
  variables <- unlist(variables)
  args <- strsplit(unlist(command.args), split='=')
  keys <- vector("character")
  values <- list()
  if (length(args) > 0) {
    for (i in 1:length(args)) {
      key <- args[[i]][1]
      value <- args[[i]][2]
      if (!(key %in% variables)) stop("\nUnrecognized option [",key,"].\n\n")
      if (grepl("\\)$", value) || grepl(":", value)) {
        internal.value <- eval(parse(text = value))
      }
      else {
        internal.value <- as.vector(value, mode=storage.mode(get(key, parent.env)))
      }
      keys <- c(keys, key)
      values <- c(values, list(internal.value))
    }
  }
  return(list("keys"=keys, "values"=values))
}

## process command-line arguments
command.args <- ProcessCommandArgs(list(commandArgs(TRUE)), list(ls()), globalenv())
keys <- command.args$keys
if (length(keys) > 0) {
  for (i in 1:length(keys)) { assign(keys[i], command.args$values[[i]]) }
}

## -------------------------------------
## ----------> MAIN FUNCTION <----------
## -------------------------------------

## get tsv files
tsv_files <- strsplit(tsv_paths_str, ',')[[1]]
n_total_wells <- length(tsv_files)

## read in data; copy is always included by default
data <- readInData(
    results_tsv_files_array=tsv_files, 
    template_id='inputTemplateEstimate', 
    nthreads = 1
)
# mark templates that don't meet the min_reads requirement
# this is done to filter out low-count seqs template data later
for (i in 1:n_total_wells){
    data[[i]] <- data[[i]] %>% 
    mutate(inputTemplateEstimate = ifelse(as.integer(copy)>=min_reads,
                                   inputTemplateEstimate, '-1'))
}

## SKIP: get excluded wells

## parse tsv files into seqinfo
seqinfo <- sequenceCollapser::parseResultsTsvFiles(
    df=data, 
    germline=data.frame(), 
    count_label='copy', 
    excluded_wells=integer(), 
    min_reads=min_reads, 
    productive_only=FALSE, 
    count_germline_mismatches=FALSE)
seqinfo$mean_templates <- seqinfo$templateCounts %>% 
                        strsplit(",") %>% 
                        purrr::map_dbl(~as.numeric(.x) %>% mean)
seqinfo$n_wells <- seqinfo$occupiedWells %>% 
                stringr::str_count(",") %>% 
                   {. + 1}
seqinfo <- dplyr::arrange(
    seqinfo, 
    desc(n_wells), 
    desc(mean_templates), 
    nucleotide
)
seqinfo_pre_collapse <- seqinfo

## error collapse
collapsed <- printFilteredSequenceInfo(
    seqinfo_pre_collapse, 
    cdr3_distance_fraction = cdr3_distance_fraction, 
    return_child_mapping = TRUE
)
seqinfo_post_collapse <- collapsed[[1]] # this just removes error derivative rows
error_collapse_map <-collapsed[[2]]
seqinfo_post_collapse_merged <- errorMerge( # merge counts
    collapsed_seqinfo = seqinfo_post_collapse, 
    precollapsed_seqinfo = seqinfo_pre_collapse, 
    collapse_map = error_collapse_map
)

## seqinfo for template counts
seqinfo_template <- sequenceCollapser::parseResultsTsvFiles(
    df=data, 
    germline=data.frame(), 
    count_label='inputTemplateEstimate', 
    excluded_wells=integer(), 
    min_reads=0, 
    productive_only=FALSE, 
    count_germline_mismatches=FALSE)
seqinfo_template$mean_templates <- seqinfo_template$templateCounts %>% 
                        strsplit(",") %>% 
                        purrr::map_dbl(~as.numeric(.x) %>% mean)
seqinfo_template$n_wells <- seqinfo_template$occupiedWells %>% 
                stringr::str_count(",") %>% 
                   {. + 1}
seqinfo_template <- dplyr::arrange(
    seqinfo_template, 
    desc(n_wells), 
    desc(mean_templates), 
    nucleotide
)
seqinfo_pre_collapse_template <- seqinfo_template

seqinfo_post_collapse_template <- 
    seqinfo_pre_collapse_template %>%
    dplyr::left_join(error_collapse_map, by = c("nucleotide" = "child")) %>%
    dplyr::filter(is.na(parent)) %>%
    dplyr::select(-parent)

seqinfo_post_collapse_merged_template <- errorMerge( # merge counts
    collapsed_seqinfo = seqinfo_post_collapse_template, 
    precollapsed_seqinfo = seqinfo_pre_collapse_template, 
    collapse_map = error_collapse_map
)

# unique seqs pre, post
seqs_pre_post <- data.frame(
    type=c('pre', 'post'), 
    count=c(dim(seqinfo_pre_collapse)[1], dim(seqinfo_post_collapse)[1]), 
    stringsAsFactors=FALSE
)
# well_num to name map
well_name_map <- data.frame(
    well_num = 1:length(tsv_files),
    well_name = tsv_files
)

## write to file
data.table::fwrite(
    error_collapse_map, 
    file = file.path(output_dir, 'error_collapse.map'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)

data.table::fwrite(
    well_name_map, 
    file = file.path(output_dir, 'well_name.map'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)

data.table::fwrite(
    seqs_pre_post, 
    file = file.path(
        output_dir, 'unique_sequences_pre_post.txt'
    ), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)

###### optional #####
data.table::fwrite(
    seqinfo_post_collapse_merged, 
    file = file.path(output_dir, 'sequence_info_post_collapse_reads.txt'),
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqinfo_pre_collapse, 
    file = file.path(output_dir, 'sequence_info_pre_collapse_reads.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqinfo_post_collapse_merged_template, 
    file = file.path(output_dir, 'sequence_info_post_collapse_templates.txt'),
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqinfo_pre_collapse_template, 
    file = file.path(output_dir, 'sequence_info_pre_collapse_templates.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)