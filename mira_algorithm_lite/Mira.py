import numpy as np
import pandas as pd
import os
import subprocess
import re
import configparser


class Mira(object):
    def __init__(self, config_path):
        self.config = configparser.ConfigParser(inline_comment_prefixes="#")
        self.config.read(config_path)
        self.rscript_dir = os.path.dirname(__file__)

    def run_error_collapse(self, tsv_paths, output_dir):
        """Run error collapse.
        Args:
            tsv_paths (list): list of tsv files
            output_dir (str): output directory for error collapsed seqinfo files
        Output:
            Save seqinfo, error map, well map files to output_dir
        """
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        tsv_paths_str = ",".join(tsv_paths)

        command = [
            "Rscript",
            os.path.join(self.rscript_dir, "run_error_collapse.r"),
            "tsv_paths_str=" + tsv_paths_str,
            "output_dir=" + output_dir,
            "cdr3_distance_fraction=" + self.config["error_collapse"]["cdr3_dist_frac"],
            "min_reads=" + self.config["error_collapse"]["min_reads_per_seq_per_well"],
        ]
        subprocess.run(command)

    def get_gene_annotations(self, tsv_paths, target_nucleotides):
        """Get gene annotations for the target nucleotides from results tsv files.
        Args:
            tsv_paths (list): list of tsv paths
            target_nucleotides (list): list of nucleotides to get gene annotations for
        Output:
            annotations (df): dataframe containing gene annotations for each nucleotide
        """
        cols = [
            "nucleotide",
            "vFamilyName",
            "vGeneName",
            "vGeneAllele",
            "vFamilyTies",
            "vGeneNameTies",
            "vGeneAlleleTies",
            "dFamilyName",
            "dGeneName",
            "dGeneAllele",
            "dFamilyTies",
            "dGeneNameTies",
            "dGeneAlleleTies",
            "jFamilyName",
            "jGeneName",
            "jGeneAllele",
            "jFamilyTies",
            "jGeneNameTies",
            "jGeneAlleleTies",
        ]
        df_list = []
        for tsv in tsv_paths:
            df = pd.read_csv(tsv, sep="\t", comment="#", low_memory=False)
            df = df[df["nucleotide"].isin(target_nucleotides)][cols]
            df_list.append(df.copy())

        df = pd.concat(df_list, axis=0)
        df.drop_duplicates("nucleotide", inplace=True)
        return df

    def make_mirage_count_table(
        self, tsv_paths, seqinfo_path, well_name_path, count_table_path
    ):
        """Make count table for mirage.
        Args:
            tsv_paths (list): list of tsv paths
            seqinfo_path (str): path to error collapsed seq info file
            well_name_path (str): path to the file containing well_num to pool_name mappings
            count_table_path (str): path to count table output
        Output:
            Save count table in count_table_path
        """

        # load seqinfo
        seq_info = pd.read_csv(
            seqinfo_path,
            sep="\t",
            dtype={"occupiedWells": str, "templateCounts": str},
        )
        # load well name map
        well_name_map = pd.read_csv(well_name_path, sep="\t")
        n_total_wells = len(well_name_map)

        # populate count table
        count_table = np.zeros((seq_info.shape[0], n_total_wells), int)
        for i, row in seq_info.iterrows():
            cols = [int(x) - 1 for x in row["occupiedWells"].split(",")]
            vals = [int(x) for x in row["templateCounts"].split(",")]
            count_table[i, cols] = vals

        # set column names
        well_names_clean = [
            x.split("/")[-1].split(".tsv")[0] for x in well_name_map["well_name"]
        ]
        col_names = ["Count_" + x + "_error_collapsed" for x in well_names_clean]

        # make count df
        count_df = pd.DataFrame(count_table, columns=col_names).astype(int)

        # combine with meta info
        count_df_final = pd.concat(
            [seq_info[["nucleotide", "aminoAcid"]], count_df],
            axis=1,
        )
        gene_annotations = self.get_gene_annotations(tsv_paths, seq_info["nucleotide"])
        count_df_final = count_df_final.merge(
            gene_annotations, how="left", on="nucleotide"
        )

        # save
        count_df_final.to_csv(
            count_table_path,
            sep="\t",
            index=False,
        )

    def run_mirage(
        self,
        tsv_paths,
        error_collapse_dir,
        antigen_key_path,
        output_dir,
        outfile_prefix,
    ):
        """Run MIRAGE.
        Args:
            tsv_paths (list): list of tsv paths
            error_collapse_dir (str): directory containing error collapsed files
            antigen_key_path (str): path to antigen key
            output_dir (str): directory to contain MIRAGE output files
            outfile_prefix (str): prefix for MIRAGE output files
        Output:
            Save MIRAGE input and output files to output_dir/mirage_prefix_*
        """
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # make count table
        seqinfo_path = os.path.join(
            error_collapse_dir, "sequence_info_post_collapse_templates.txt"
        )
        well_name_path = os.path.join(error_collapse_dir, "well_name.map")
        count_table_path = os.path.join(output_dir, outfile_prefix + "_count_table.txt")
        self.make_mirage_count_table(
            tsv_paths, seqinfo_path, well_name_path, count_table_path
        )

        # run MIRAGE
        use_nopep_r_str = (
            "TRUE" if self.config["mirage"]["use_nopep"].upper() == "TRUE" else "FALSE"
        )
        use_unsort_r_str = (
            "TRUE" if self.config["mirage"]["use_unsort"].upper() == "TRUE" else "FALSE"
        )
        estimate_sort_unsort_ratio_r_str = (
            "TRUE"
            if self.config["mirage"]["estimate_sort_unsort_ratio"].upper() == "TRUE"
            else "FALSE"
        )
        use_all_addresses_r_str = (
            "TRUE"
            if self.config["mirage"]["use_all_addresses"].upper() == "TRUE"
            else "FALSE"
        )

        command = [
            "Rscript",
            os.path.join(self.rscript_dir, "run_mirage.r"),
            "mira_count_table_path=" + count_table_path,
            "num_total_wells=" + self.config["mirage"]["num_total_wells"],
            "num_antigen_wells=" + self.config["mirage"]["num_antigen_wells"],
            "output_path_prefix=" + os.path.join(output_dir, outfile_prefix),
            "key_path=" + antigen_key_path,
            "use_nopep=" + use_nopep_r_str,
            "eps=" + self.config["mirage"]["eps"],
            "use_unsort=" + use_unsort_r_str,
            "estimate_sort_unsort_ratio=" + estimate_sort_unsort_ratio_r_str,
            "default_sort_unsort_ratio="
            + self.config["mirage"]["default_sort_unsort_ratio"],
            "num_cells_unsort=" + self.config["mirage"]["num_cells_unsort"],
            "use_all_addresses=" + use_all_addresses_r_str,
        ]
        subprocess.run(command)

    def check_input_args(self, tsv_paths, antigen_key_path):
        """Check input arguments.
        Args:
            tsv_paths (list): list of tsv paths
            antigen_key_path (str): path to antigen key
        Output:
            Raise exception if any of the arguments are illegal
        """
        if len(tsv_paths) == 0:
            raise Exception("No TSV paths provided")
        bad_tsvs = [x for x in tsv_paths if not os.path.exists(x)]
        if len(bad_tsvs) > 0:
            raise Exception("These TSV files do not exist: {}".format(bad_tsvs))
        if not os.path.exists(antigen_key_path):
            raise Exception(
                "This antigen key does not exist: {}".format(antigen_key_path)
            )

    def run_mira_algorithm(
        self, tsv_paths, antigen_key_path, output_dir, mirage_outfile_prefix
    ):
        """
        Run error collapsing and MIRAGE.
        Args:
            tsv_paths (list): list of absolute paths to results tsv files
            antigen_key_path (str): path to antigen key
            output_dir (str): directory to hold output files
            mirage_outfile_prefix (str): prefix for mirage outfiles
        Output:
            Saves error collapse files to output_dir/error_collapse_files/ and
            MIRAGE output files to output_dir/mirage_results/
        """
        # check input
        self.check_input_args(tsv_paths, antigen_key_path)

        # run error collapse
        error_collapse_output_dir = os.path.join(output_dir, "error_collapse_files")
        self.run_error_collapse(tsv_paths, error_collapse_output_dir)

        # run mirage
        mirage_output_dir = os.path.join(output_dir, "mirage_results")
        self.run_mirage(
            tsv_paths,
            error_collapse_output_dir,
            antigen_key_path,
            mirage_output_dir,
            mirage_outfile_prefix,
        )
