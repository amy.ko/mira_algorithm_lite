library(MIRAGE)

## Run MIRAGE
# Output: standard MIRAGE outfiles

## input arguments
mira_count_table_path <- "" # [REQUIRED] path to MIRA count table
num_total_wells <- 0 # [REQUIRED] total number of wells
num_antigen_wells <- 0 # [REQUIRED] number of lettered pools
output_path_prefix <-"" # [REQUIRED] directory + prefix for output files
key_path <- "" # [REQUIRED] path to the key file
use_nopep <- TRUE # <optional> true if you want to use the no-peptide pools
eps <- .01 # <optional> A small fudge factor, which can be thought of as the cell leakage rate (do not change unless you know what you are doing)
use_unsort <- TRUE # <optional> true if you want to use the unsorted pool in the statistical model
estimate_sort_unsort_ratio <- use_unsort # <optional>
default_sort_unsort_ratio <- 10 # <optional> 
num_cells_unsort <- 1e7 # <optional> number of input cells in the unsorted pool
use_all_addresses <- TRUE # <optional> true if you want to use all potential addresses to compute the FDR

        
## -------------------------------------
## ---> INTERNAL UTILITY FUNCTIONS <----
## -------------------------------------

## function for parsing command line arguments: first two inputs are character
## vectors embedded in lists; third input is environment of the calling context
ProcessCommandArgs <- function(command.args, variables, parent.env) {
  variables <- unlist(variables)
  args <- strsplit(unlist(command.args), split='=')
  keys <- vector("character")
  values <- list()
  if (length(args) > 0) {
    for (i in 1:length(args)) {
      key <- args[[i]][1]
      value <- args[[i]][2]
      if (!(key %in% variables)) stop("\nUnrecognized option [",key,"].\n\n")
      if (grepl("\\)$", value) || grepl(":", value)) {
        internal.value <- eval(parse(text = value))
      }
      else {
        internal.value <- as.vector(value, mode=storage.mode(get(key, parent.env)))
      }
      keys <- c(keys, key)
      values <- c(values, list(internal.value))
    }
  }
  return(list("keys"=keys, "values"=values))
}

## -------------------------------------
## ----------> MAIN FUNCTION <----------
## -------------------------------------

## process command-line arguments
command.args <- ProcessCommandArgs(list(commandArgs(TRUE)), list(ls()), globalenv())
keys <- command.args$keys
if (length(keys) > 0) {
  for (i in 1:length(keys)) { assign(keys[i], command.args$values[[i]]) }
}

run_mirage(
    mira_count_table_path=mira_count_table_path,
    num_total_wells=num_total_wells,
    num_antigen_wells=num_antigen_wells,
    output_path_prefix=output_path_prefix,
    key_path=key_path,
    use_nopep=use_nopep,
    eps=eps,
    use_unsort=use_unsort,
    estimate_sort_unsort_ratio=estimate_sort_unsort_ratio,
    default_sort_unsort_ratio=default_sort_unsort_ratio,
    num_cells_unsort=num_cells_unsort,
    use_all_addresses=use_all_addresses,
)
