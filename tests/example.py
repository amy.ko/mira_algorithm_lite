import mira_algorithm_lite.Mira as Mira
import os
import glob

# input directories and constants
project_dir = "/home/dna.corp.adaptivebiotech.com/ako/maapseq/"
tsv_dir = "/mnt/tcr_discovery/MIRA_data/SEQ/P02-429/SEQ/eSR053_12112_3024051/"
config_path = os.path.join(project_dir, "configs", "eSR053_mira_config.txt")
antigen_key_path = os.path.join(project_dir, "antigen_keys", "MIRA_ID141_11ch6_KEY.txt")
output_dir = "my_output_dir"
mirage_outfile_prefix = "my_mirage_output_name"

# initialize MIRA object
mira = Mira.Mira(config_path)

# tsv paths for this sample
sample = "eSR053"
tsv_paths = sorted(
    [x for x in glob.glob(tsv_dir + "*results.tsv.gz") if 'nopeptide2' not in x]
)

# run error collapse + MIRAGE
mira.run_mira_algorithm(tsv_paths, antigen_key_path, output_dir, mirage_outfile_prefix)
