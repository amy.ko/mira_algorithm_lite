# About
mira_algorithm_lite is a Python package that runs error collapse and MIRAGE on immunoSEQ MIRA data. It takes in results.tsv files and outputs results from error collapse and MIRAGE. 
## Input
- results.tsv files 
- antigen key file

## Output
- error_collapse_files/
    - sequence_info_pre_collapse_templates.txt: sequence info for templates before error collapse
    - sequence_info_post_collapse_templates.txt: sequence info for templates after error collapse 
    - sequence_info_pre_collapse_reads.txt: sequence info for reads before error collapse
    - sequence_info_post_collapse_reads.txt: sequence info for reads after error collapse
    - error_collapse.map: parent to child sequence map
    - unique_sequences_pre_post.txt: number of unique sequences before and after collapse
    - well_name.map: well number to antigen pool map
- mirage_results/
    - *_count_table.txt: count table used in MIRAGE
    - output files from MIRAGE


# Installation
## Install R dependencies
Install [sequenceCollapser](https://gitlab.com/adaptivebiotech/comp-bio/stat/tools/sequencecollapser) and [MIRAGE](https://gitlab.com/adaptivebiotech/comp-bio/stat/tools/mirage). In an R-console, run the following commands:
```
library(devtools)
devtools::install_git(
    "git@gitlab.com:adaptivebiotech/comp-bio/stat/tools/sequencecollapser.git"
)
devtools::install_git(
    "git@gitlab.com:adaptivebiotech/comp-bio/stat/tools/mirage.git"
)

```

## Install mira_algorithm_lite
Clone this repository and run "pip install ." inside the repository where *setup.py* is. 
```
git clone git@gitlab.com:amy.ko/mira_algorithm_lite.git
cd mira_algorithm_lite
pip install .
```

# How to use
## Make a config.txt file
Copy mira_algorithm_lite/tests/config.txt and edit it for your use case. Most of the time, you'll only have to change *num_total_wells* and *num_antigen_wells*. 
```
[error_collapse]
cdr3_dist_frac = .035               # Max allowed distance between parent and child
min_reads_per_seq_per_well = 1      # Zero out counts that are less than this threshold. 

[mirage]
num_total_wells = 11                # e.g. 11 for a 11 choose 6 MIRA
num_antigen_wells = 6               # e.g. 6 for a 11 choose 6 MIRA
use_nopep = True                    # True if you want to use the nopep pool in the analysis
use_unsort = True                   # True if you want to use the unsorted pool in the analysis
estimate_sort_unsort_ratio = True   # True if you want to estimate sort/unsort
use_all_addresses = True            # True if you want to use all addresses to compute the FDR
eps = .01                           # Do not change unless you know what you're doing
default_sort_unsort_ratio = 10      # Do not change unless you know what you're doing
num_cells_unsort = 1e7              # Do not change unless you know what you're doing
```

## Run code
```
import mira_algorithm_lite.Mira as Mira

## Specify input and output paths
config_path = "my_config.txt"
tsv_paths = ["well1.results.tsv", "well2.results.tsv"]
antigen_key_path = "my_antigen_key.txt"
output_dir = "my_output_dir"
mirage_outfile_prefix = "my_mirage_run_name"

## Initialize Mira object
mira = Mira.Mira(config_path)

## Run MIRA algorithm
mira.run_mira_algorithm(tsv_paths, antigen_key_path, output_dir, mirage_outfile_prefix)
```