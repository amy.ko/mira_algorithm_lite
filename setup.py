from distutils.core import setup

setup(
    name='mira_algorithm_lite',
    version='1.0',
    description='Wrapper functions to run error collapse and MIRAGE on results.tsv data',
    author='Amy Ko',
    author_email='ako@adaptivebiotech.com',
    packages=['mira_algorithm_lite'],
    install_requires=['numpy>=1.22.3', 'pandas>=1.4.2'],
    package_data={'': ['run_error_collapse.r', 'run_mirage.r']},
    include_package_data=True
)
